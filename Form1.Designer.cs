﻿
namespace Single_Chat
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxСhatWindow = new System.Windows.Forms.ListBox();
            this.textBoxChatWindow = new System.Windows.Forms.TextBox();
            this.enter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxСhatWindow
            // 
            this.listBoxСhatWindow.FormattingEnabled = true;
            this.listBoxСhatWindow.Location = new System.Drawing.Point(141, 63);
            this.listBoxСhatWindow.Name = "listBoxСhatWindow";
            this.listBoxСhatWindow.Size = new System.Drawing.Size(314, 212);
            this.listBoxСhatWindow.TabIndex = 0;
            // 
            // textBoxChatWindow
            // 
            this.textBoxChatWindow.Location = new System.Drawing.Point(141, 298);
            this.textBoxChatWindow.Multiline = true;
            this.textBoxChatWindow.Name = "textBoxChatWindow";
            this.textBoxChatWindow.Size = new System.Drawing.Size(314, 60);
            this.textBoxChatWindow.TabIndex = 1;
            // 
            // enter
            // 
            this.enter.Location = new System.Drawing.Point(257, 378);
            this.enter.Name = "enter";
            this.enter.Size = new System.Drawing.Size(75, 23);
            this.enter.TabIndex = 2;
            this.enter.Text = "Ввод";
            this.enter.UseVisualStyleBackColor = true;
            this.enter.Click += new System.EventHandler(this.enter_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.enter);
            this.Controls.Add(this.textBoxChatWindow);
            this.Controls.Add(this.listBoxСhatWindow);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxСhatWindow;
        private System.Windows.Forms.TextBox textBoxChatWindow;
        private System.Windows.Forms.Button enter;
    }
}

